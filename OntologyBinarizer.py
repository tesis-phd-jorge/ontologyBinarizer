#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 23 11:21:14 2019

@author: Jorge Chamorro Padial
"""

from collections import deque
from math import log2

from anytree import Node
from anytree.exporter import DotExporter


class OntologyBinarizer(object):
    """
    This class builds a Tree structure and generate an unique numeric value for each node so that
    the more significant bits corresponds to the father of a node.
    """

    def __init__(self):
        """
        Default constructor
        """

        self._root = Node("root")
        self._level_width_list = []
        self._token_dictionary = {}

    def get_root(self):
        """
        Returns the root node.
        :return: The root node of the tree.
        :rtype: Node
        """

        return self._root

    def _get_maximum_brothers_of_level(self, list_of_father_brother_nodes : list):
        """
        Returns the maximun number of brother Nodes of the next level.

        :param list_of_father_brother_nodes: list of parent brother Nodes
        :type list_of_father_brother_nodes: list
        :return: The width of the level.
        :rtype: int
        """

        return max([len(node.children) for node in list_of_father_brother_nodes])

    def get_token_dictionary(self):
        """
        Returns the token dictionary.
        :return: The token dictionary.
        :rtype: dict
        """

        return self._token_dictionary

    def add_child(self, node_content : str, node_parent : Node):
        """
        Add a new child for a Node. Instances a new Node object and
        returns a reference to it.
        :param node_content: The value of the node.
        :type node_content: str
        :param node_parent: The parent node.
        :type node_parent: Node
        :return: A reference to new Node created.
        """
        return Node(node_content, parent=node_parent)

    def export_tree_to_image(self, file_name="tree.png"):
        """
        Generates a png file with a graphic representation of the tree.
        :param file_name: The filename
        :type file_name: str
        """
        DotExporter(self._root).to_picture(file_name)

    def _compute_tree_width(self):
        """
        Compute the tree maximum number of brother nodes for each level.
        """
        current_parent_nodes = deque()
        current_parent_nodes.append(self._root)

        for level in range(self._root.height):
            max_level_width = 0
            level_parent_nodes = list()
            while (len(current_parent_nodes) > 0):
                parent_node = current_parent_nodes.pop()
                list_of_children = parent_node.children
                width_parent_node = len(list_of_children)
                max_level_width = max(max_level_width, width_parent_node)
                level_parent_nodes.extend(list_of_children)
            current_parent_nodes.extend(level_parent_nodes)
            self._level_width_list.append(int(log2(max_level_width) + 1))

    def _tokenize(self, parent_token : Node, list_of_level_nodes : list, level : int):
        """
        Generate numeric tokens for each node of the tree and add them to a token dictionary.

        :param parent_token: The parent node of list_of_level_nodes.
        :type parent_token: Node
        :param list_of_level_nodes: The list of children level nodes.
        :type list_of_level_nodes: list.
        :param level: The current level.
        :type level: int
        """
        partial_width = sum(self._level_width_list[level + 1:])
        current_partial_token = 1

        for node in list_of_level_nodes:
            current_token = parent_token | (current_partial_token << (partial_width))
            self._token_dictionary[node.name] = current_token
            current_partial_token = current_partial_token + 1
            if not node.is_leaf:
                list_of_children_nodes = node.children
                self._tokenize(current_token, list_of_children_nodes, level + 1)

    def compute_tree(self):
        """
        Tokenizes each node of the tree. Explores the tree to get the maximun number of brother nodes of each level
        and then, generate an unique token for each Node.
        """

        self._compute_tree_width()
        self._tokenize(0, self._root.children, 0)


